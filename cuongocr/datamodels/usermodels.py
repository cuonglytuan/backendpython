from google.appengine.ext import endpoints
from google.appengine.ext import ndb

from webapp2_extras import security
from google.appengine.api import users


TIME_FORMAT_STRING = '%b %d, %Y %I:%M:%S %p'


def get_endpoints_current_user(raise_unauthorized=True):
    """Returns a current user and (optionally) causes an HTTP 401 if no user.

    Args:
        raise_unauthorized: Boolean; defaults to True. If True, this method
            raises an exception which causes an HTTP 401 Unauthorized to be
            returned with the request.

    Returns:
        The signed in user if there is one, else None if there is no signed in
        user and raise_unauthorized is False.
    """
    current_user = endpoints.get_current_user()
    if raise_unauthorized and current_user is None:
        raise endpoints.UnauthorizedException('Invalid token.')
    return current_user


class UserToken(ndb.Model):
    """Stores validation tokens for users."""

    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    userid = ndb.IntegerProperty(required=True)
    subject = ndb.StringProperty(required=True)
    token = ndb.StringProperty(required=True)

    @classmethod
    def get_key(cls, userid, subject, token):
        """Returns a token key.

        :param user:
            User unique ID.
        :param subject:
            The subject of the key. Examples:

            - 'auth'
            - 'signup'
        :param token:
            Randomly generated token.
        :returns:
            ``model.Key`` containing a string id in the following format:
            ``{user_id}.{subject}.{token}``
        """
        return ndb.Key(cls, '%s.%s.%s' % (userid, subject, token))

    @classmethod
    def create(cls, userid, subject, token=None):
        """Creates a new token for the given user.

        :param user:
            User unique ID.
        :param subject:
            The subject of the key. Examples:

            - 'auth'
            - 'signup'
        :param token:
            Optionally an existing token may be provided.
            If None, a random token will be generated.
        :returns:
            The newly created :class:`UserToken`.
        """
        token = token or security.generate_random_string(entropy=128)
        entity = cls(userid=userid, subject=subject, token=token)
        entity.put()
        return entity

    @classmethod
    def get(cls, userid=None, subject=None, token=None):
        """Fetches a user token.

        :param user:
            User unique ID.
        :param subject:
            The subject of the key. Examples:

            - 'auth'
            - 'signup'
        :param token:
            The existing token needing verified.
        :returns:
            A :class:`UserToken` or None if the token does not exist.
        """
        if userid and subject and token:
            return cls.get_key(userid, subject, token).get()

        assert subject and token, \
            u'subject and token must be provided to UserToken.get().'
        return cls.query(cls.subject == subject, cls.token == token).get()


    @classmethod
    def query_current_user(cls, nameofuser):
        """Creates a query for the scores of the current user.

        Returns:
            An ndb.Query object bound to the current user. This can be used
            to filter for other properties or order by them.
        """
        return cls.query(cls.userid == nameofuser)


class UserModels(ndb.Model):
    """Model to store scores that have been inserted by users.

    Since the played property is auto_now_add=True, Scores will document when
    they were inserted immediately after being stored.
    """
    username = ndb.UserProperty(required=True)
    email = ndb.StringProperty(required=True)
    password = ndb.PickleProperty(required=True)
    fullname = ndb.StringProperty(required=True)
    birthday = ndb.StringProperty(required=True)
    mobile = ndb.StringProperty()
    address = ndb.StringProperty()
    scoreRight = ndb.IntegerProperty()
    scoreWrong = ndb.IntegerProperty()
    created_at = ndb.DateTimeProperty(auto_now_add=True)
    #outcome = ndb.StringProperty(required=True)
    #played = ndb.DateTimeProperty(auto_now_add=True)
    #player = ndb.UserProperty(required=True)

    def set_password(self, raw_password):
      """Sets the password for the current user
    
      :param raw_password:
          The raw password which will be hashed and stored
      """
      self.password = security.generate_password_hash(raw_password, length=12)

    @property
    def timestamp(self):
        """Property to format a datetime object to string."""
        return self.played.strftime(TIME_FORMAT_STRING)

    def to_message(self):
        """Turns the Score entity into a ProtoRPC object.

        This is necessary so the entity can be returned in an API request.

        Returns:
            An instance of ScoreResponseMessage with the ID set to the datastore
            ID of the current entity, the outcome simply the entity's outcome
            value and the played value equal to the string version of played
            from the property 'timestamp'.
        """

    @classmethod
    def put_from_message(cls, message):
        """Gets the current user and inserts a score.

        Args:
            message: A ScoreRequestMessage instance to be inserted.

        Returns:
            The Score entity that was inserted.
        """
        #current_user = get_endpoints_current_user()
        entity = cls(username=users.User(message.email), email=message.email,
                     password=security.generate_password_hash(message.password, length=12), fullname=message.fullname,
                      birthday=message.birthday, mobile=message.mobile, address=message.address, scoreRight=0, scoreWrong=0)
        entity.put()
        return entity

    @classmethod
    def query_current_user(cls, nameofuser):
        """Creates a query for the scores of the current user.

        Returns:
            An ndb.Query object bound to the current user. This can be used
            to filter for other properties or order by them.
        """
        return cls.query(cls.email == nameofuser)
