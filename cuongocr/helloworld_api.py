"""Hello World API implemented using Google Cloud Endpoints.

Defined here are the ProtoRPC messages needed to define Schemas for methods
as well as those methods defined in an API.
"""

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

from google.appengine.ext.webapp import template
from google.appengine.ext import ndb

from datetime import datetime
from datamodels.usermodels import UserModels
from datamodels.usermodels import UserToken

import logging
import os.path
import webapp2

from webapp2_extras import auth
from webapp2_extras import sessions

from webapp2_extras.auth import InvalidAuthIdError
from webapp2_extras.auth import InvalidPasswordError

from webapp2_extras import security
from google.appengine.api import users

WEB_CLIENT_ID = 'replace this with your web client application ID'
ANDROID_CLIENT_ID = 'replace this with your Android client ID'
IOS_CLIENT_ID = 'replace this with your iOS client ID'
ANDROID_AUDIENCE = WEB_CLIENT_ID

package = 'Hello'

TIME_FORMAT_STRING = '%b %d, %Y %I:%M:%S %p'


def get_endpoints_current_user(raise_unauthorized=True):
    """Returns a current user and (optionally) causes an HTTP 401 if no user.

    Args:
        raise_unauthorized: Boolean; defaults to True. If True, this method
            raises an exception which causes an HTTP 401 Unauthorized to be
            returned with the request.

    Returns:
        The signed in user if there is one, else None if there is no signed in
        user and raise_unauthorized is False.
    """
    current_user = endpoints.get_current_user()
    if raise_unauthorized and current_user is None:
        raise endpoints.UnauthorizedException('Invalid token.')
    return current_user


class Userpost(messages.Message):
    """User sign up informations."""
    email = messages.StringField(1, required=True)
    password = messages.StringField(2, required=True)
    fullname = messages.StringField(3, required=True)
    birthday = messages.StringField(4, required=True)
    mobile = messages.StringField(5)
    address = messages.StringField(6)


class UserpostLogin(messages.Message):
    """User sign up informations."""
    email = messages.StringField(1, required=True)
    password = messages.StringField(2, required=True)


class Greeting(messages.Message):
    """Greeting that stores a message."""
    message = messages.StringField(1)


class SignUpResponse(messages.Message):
    """Greeting that stores a message."""
    status = messages.StringField(1)
    userid = messages.IntegerField(2)
    token = messages.StringField(3)
    fullname = messages.StringField(4)
    scoreRight = messages.IntegerField(5)
    scoreWrong = messages.IntegerField(6)


class GreetingCollection(messages.Message):
    """Collection of Greetings."""
    items = messages.MessageField(Greeting, 1, repeated=True)


def user_required(handler):
    """
      Decorator that checks if there's a user associated with the current session.
      Will also fail if there's no session present.
    """
    def check_login(self, *args, **kwargs):
      auth = self.auth
      if not auth.get_user_by_session():
        self.redirect(self.uri_for('login'), abort=True)
      else:
        return handler(self, *args, **kwargs)
    
    return check_login

STORED_GREETINGS = GreetingCollection(items=[
    Greeting(message='hello world!'),
    Greeting(message='goodbye world!'),
])

@endpoints.api(name='helloworld', version='v1',
               allowed_client_ids=[WEB_CLIENT_ID, ANDROID_CLIENT_ID,
                                   IOS_CLIENT_ID, endpoints.API_EXPLORER_CLIENT_ID],
               audiences=[ANDROID_AUDIENCE],
               scopes=[endpoints.EMAIL_SCOPE])
class HelloWorldApi(remote.Service):
    """helloworld API v1."""

    config = {
      'webapp2_extras.auth': {
        'user_model': 'models.User',
        'user_attributes': ['name']
      },
      'webapp2_extras.sessions': {
        'secret_key': 'YOUR_SECRET_KEY'
      }
    }
    
    @webapp2.cached_property
    def auth(self):
      """Shortcut to access the auth instance as a property."""
      return auth.get_auth()
    
    @webapp2.cached_property
    def user_info(self):
      """Shortcut to access a subset of the user attributes that are stored
      in the session.
    
      The list of attributes to store in the session is specified in
        config['webapp2_extras.auth']['user_attributes'].
      :returns
        A dictionary with most user information
      """
      return self.auth.get_user_by_session()
    
    @webapp2.cached_property
    def user(self):
      """Shortcut to access the current logged in user.
    
      Unlike user_info, it fetches information from the persistence layer and
      returns an instance of the underlying model.
    
      :returns
        The instance of the user model associated to the logged in user.
      """
      u = self.user_info
      return self.user_model.get_by_id(u['user_id']) if u else None
    
    @webapp2.cached_property
    def user_model(self):
      """Returns the implementation of the user model.
    
      It is consistent with config['webapp2_extras.auth']['user_model'], if set.
      """    
      return self.auth.store.user_model
    
    @webapp2.cached_property
    def session(self):
        """Shortcut to access the current session."""
        return self.session_store.get_session(backend="datastore")
    
    def render_template(self, view_filename, params=None):
        if not params:
          params = {}
        user = self.user_info
        params['user'] = user
        path = os.path.join(os.path.dirname(__file__), 'views', view_filename)
        self.response.out.write(template.render(path, params))
    
    def display_message(self, message):
        """Utility function to display a template with a simple message."""
        params = {
          'message': message
        }
        self.render_template('message.html', params)
    
    # this is needed for webapp2 sessions to work
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)
        
        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)

    MULTIPLY_METHOD_RESOURCE = endpoints.ResourceContainer(
            Greeting,
            times=messages.IntegerField(2, variant=messages.Variant.INT32,
                                        required=True))

    @endpoints.method(MULTIPLY_METHOD_RESOURCE, Greeting,
                      path='hellogreeting/{times}', http_method='POST',
                      name='greetings.multiply')
    def greetings_multiply(self, request):
        return Greeting(message=request.message * request.times)


    @endpoints.method(message_types.VoidMessage, GreetingCollection,
                      path='hellogreeting', http_method='GET',
                      name='greetings.listGreeting')
    def greetings_list(self, unused_request):
        return STORED_GREETINGS

    ID_RESOURCE = endpoints.ResourceContainer(
            message_types.VoidMessage,
            id=messages.IntegerField(1, variant=messages.Variant.INT32))

    @endpoints.method(ID_RESOURCE, Greeting,
                      path='hellogreeting/{id}', http_method='GET',
                      name='greetings.getGreeting')
    def greeting_get(self, request):
        try:
            return STORED_GREETINGS.items[request.id]
        except (IndexError, TypeError):
            raise endpoints.NotFoundException('Greeting %s not found.' %
                                              (request.id,))
    @endpoints.method(message_types.VoidMessage, Greeting,
                      path='hellogreeting/authed', http_method='POST',
                      name='greetings.authed')
    def greeting_authed(self, request):
        current_user = endpoints.get_current_user()
        email = (current_user.email() if current_user is not None
                 else 'Anonymous')
        return Greeting(message='hello %s' % (email,))

    """ API create account. """
    ACCOUNT_SIGN_UP_RESOURCE = endpoints.ResourceContainer(
            Userpost,
            id=messages.IntegerField(1, variant=messages.Variant.INT32))

    @endpoints.method(Userpost, SignUpResponse,
                      path='account/created', http_method='POST',
                      name='account.created')
    def account_created(self, request):
        currentcv = UserModels.query_current_user(request.email)
        if currentcv is not None and currentcv.get() is None:
            entity = UserModels.put_from_message(request)
            tokenEntity = UserToken.create(entity.key.id(), 'signup', None)
            return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenEntity.token, fullname = entity.fullname, 
                                  scoreRight = entity.scoreRight, scoreWrong = entity.scoreWrong)
        else:
            return SignUpResponse(status='email already exist')

    @endpoints.method(UserpostLogin, SignUpResponse,
                      path='account/login', http_method='POST',
                      name='account.login')
    def account_login(self, request):
        currentcv = UserModels.query_current_user(request.email)
        entity = currentcv.get()
        if currentcv is not None and entity is not None:
            if security.check_password_hash(request.password, currentcv.get().password):
                currentusertoken = UserToken.query_current_user(entity.key.id())
                tokenSaveEntity = currentusertoken.get()
                if currentusertoken is not None and tokenSaveEntity is not None:
                    return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenSaveEntity.token, fullname = entity.fullname, 
                                  scoreRight = entity.scoreRight, scoreWrong = entity.scoreWrong)
                else:
                    tokenNewEntity = UserToken.create(entity.key.id(), 'login', None)
                    return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenNewEntity.token)
            else:
                return SignUpResponse(status='password invalid')
        else:
            return SignUpResponse(status='email already exist')

APPLICATION = endpoints.api_server([HelloWorldApi])